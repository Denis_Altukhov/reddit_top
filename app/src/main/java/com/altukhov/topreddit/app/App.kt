package com.altukhov.topreddit.app

import android.app.Activity
import android.app.Application
import com.altukhov.topreddit.api.dagger.DataModule
import com.altukhov.topreddit.app.dagger.AppModule
import com.altukhov.topreddit.app.dagger.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
class App: Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .dataModule(DataModule())
                .build()
                .injectApp(this)
    }


    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingActivityInjector
    }
}