package com.altukhov.topreddit.api

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
enum class Status {
    RUNNING,
    SUCCESS,
    FAILED
}

@Suppress("DataClassPrivateConstructor")
data class NetworkState private constructor(
        val status: Status,
        val msg: String? = null) {
    companion object {
        val LOADED = NetworkState(Status.SUCCESS)
        val LOADING = NetworkState(Status.RUNNING)
        fun error(msg: String?) = NetworkState(Status.FAILED, msg)
    }
}