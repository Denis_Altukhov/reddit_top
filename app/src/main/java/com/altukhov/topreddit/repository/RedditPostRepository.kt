package com.altukhov.topreddit.repository

import android.arch.lifecycle.Transformations
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.support.annotation.MainThread
import com.altukhov.topreddit.api.RedditApi
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
class RedditPostRepository
@Inject constructor(
        private val redditApi: RedditApi
) {
    @MainThread
    fun postsOfSubreddit(subredditName: String, pageSize: Int, totalItemCount: Int, compositeDisposable: CompositeDisposable): Listing {
        val sourceFactory = SubRedditDataSourceFactory(redditApi, subredditName, totalItemCount, compositeDisposable)

        val livePagedList = LivePagedListBuilder(sourceFactory, PagedList
                .Config
                .Builder()
                .setInitialLoadSizeHint(pageSize)
                .setPageSize(pageSize)
                .setEnablePlaceholders(false)
                .build())
                .build()

        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }

        return Listing(
                pagedList = livePagedList,
                networkState = Transformations.switchMap(sourceFactory.sourceLiveData, {
                    it.networkState
                }),
                postsCount = Transformations.switchMap(sourceFactory.sourceLiveData, {
                    it.limitFooter
                }),
                retry = {
                    sourceFactory.sourceLiveData.value?.retryAllFailed()
                },
                refresh = {
                    sourceFactory.sourceLiveData.value?.invalidate()
                },
                refreshState = refreshState
        )
    }
}