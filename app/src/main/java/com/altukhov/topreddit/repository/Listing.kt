package com.altukhov.topreddit.repository

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import com.altukhov.topreddit.api.NetworkState
import com.altukhov.topreddit.data.RedditPost

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
class Listing (
        val pagedList: LiveData<PagedList<RedditPost>>,
        val networkState: LiveData<NetworkState>,
        val refreshState: LiveData<NetworkState>,
        val postsCount: LiveData<Boolean>,
        val refresh: () -> Unit,
        val retry: () -> Unit)