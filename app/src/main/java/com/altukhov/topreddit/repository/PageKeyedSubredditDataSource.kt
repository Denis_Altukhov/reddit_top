package com.altukhov.topreddit.repository

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import com.altukhov.topreddit.api.NetworkState
import com.altukhov.topreddit.api.RedditApi
import com.altukhov.topreddit.data.RedditPost
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
class PageKeyedSubredditDataSource(
        private val redditApi: RedditApi,
        private val subredditName: String,
        private val totalItemCount: Int,
        private val compositeDisposable: CompositeDisposable) : PageKeyedDataSource<String, RedditPost>() {

    private var retry: (() -> Any)? = null

    val networkState = MutableLiveData<NetworkState>()

    val limitFooter = MutableLiveData<Boolean>()

    val initialLoad = MutableLiveData<NetworkState>()

    private var listSize = 0


    fun retryAllFailed() {
        val prevRetry = retry
        retry = null
        prevRetry?.let {
            Single.just(it).subscribeBy { it.invoke() }
        }
    }

    override fun loadBefore(
            params: LoadParams<String>,
            callback: LoadCallback<String, RedditPost>) {
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<String, RedditPost>) {
        networkState.postValue(NetworkState.LOADING)
        val limit = if (totalItemCount - listSize > params.requestedLoadSize) params.requestedLoadSize else totalItemCount - listSize
        if (limit > 0)
            redditApi.getTopAfter(subreddit = subredditName,
                    after = params.key,
                    limit = limit)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { compositeDisposable += it }
                    .subscribeBy(
                            onSuccess = {
                                val data = it?.data
                                listSize += data!!.children.size
                                val items = data?.children?.map { it.data }
                                if (data.after.isNullOrBlank())
                                    limitFooter.postValue(true)
                                retry = null
                                callback.onResult(items, data?.after)
                                networkState.postValue(NetworkState.LOADED)
                            },
                            onError = {
                                retry = {
                                    loadAfter(params, callback)
                                }
                                networkState.postValue(NetworkState.error(it.message ?: "unknown err"))
                            })
        else {
            limitFooter.postValue(true)
            networkState.postValue(NetworkState.LOADED)
        }

    }

    override fun loadInitial(
            params: LoadInitialParams<String>,
            callback: LoadInitialCallback<String, RedditPost>) {
        limitFooter.postValue(false)
        networkState.postValue(NetworkState.LOADING)
        initialLoad.postValue(NetworkState.LOADING)
        redditApi.getTop(subreddit = subredditName,
                limit = params.requestedLoadSize)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { compositeDisposable += it }
                .subscribeBy(
                        onSuccess = {
                            val data = it?.data
                            listSize += data!!.children.size
                            val items = data?.children?.map { it.data }
                            retry = null
                            networkState.postValue(NetworkState.LOADED)
                            initialLoad.postValue(NetworkState.LOADED)
                            if (data.after.isNullOrBlank())
                                limitFooter.postValue(true)
                            callback.onResult(items, data?.before, data?.after)
                        },
                        onError = {
                            retry = {
                                loadInitial(params, callback)
                            }
                            val error = NetworkState.error(it.message ?: "unknown error")
                            networkState.postValue(error)
                            initialLoad.postValue(error)
                        }

                )
    }
}