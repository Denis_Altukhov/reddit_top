package com.altukhov.topreddit.reddit

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.altukhov.topreddit.app.dagger.PerActivity
import com.altukhov.topreddit.repository.RedditPostRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
@PerActivity
class RedditViewModel
@Inject constructor(private val repository: RedditPostRepository) : ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    private val subredditName = MutableLiveData<String>()
    private val repoResult = Transformations.map(subredditName, {
        repository.postsOfSubreddit(it, 10, 50, compositeDisposable)
    })
    val posts = Transformations.switchMap(repoResult, { it.pagedList })!!
    val postsCount = Transformations.switchMap(repoResult, { it.postsCount })!!
    val networkState = Transformations.switchMap(repoResult, { it.networkState })!!
    val refreshState = Transformations.switchMap(repoResult, { it.networkState })!!

    fun refresh() {
        repoResult.value?.refresh?.invoke()
    }

    fun showSubreddit(subreddit: String): Boolean {
        if (subredditName.value == subreddit) {
            return false
        }
        subredditName.value = subreddit
        return true
    }

    fun retry() {
        val listing = repoResult?.value
        listing?.retry?.invoke()
    }

    fun currentSubreddit(): String? = subredditName.value

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}