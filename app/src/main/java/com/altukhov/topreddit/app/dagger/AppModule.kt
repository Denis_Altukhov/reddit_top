package com.altukhov.topreddit.app.dagger

import android.app.Application
import com.altukhov.topreddit.api.RedditApi
import com.altukhov.topreddit.repository.RedditPostRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
@Module
open class AppModule(private val app: Application) {

    @Provides
    @Singleton
    internal fun appApplication(): Application = app

    @Provides
    @Singleton
    open internal fun provideRepository(redditApi: RedditApi): RedditPostRepository = RedditPostRepository(redditApi)

}