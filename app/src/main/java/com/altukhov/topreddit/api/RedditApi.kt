package com.altukhov.topreddit.api

import com.altukhov.topreddit.data.RedditPost
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
interface RedditApi {

    @GET("/r/{subreddit}/top.json")
    fun getTop(
            @Path("subreddit") subreddit: String,
            @Query("limit") limit: Int): Single<ListingResponse>

    @GET("/r/{subreddit}/top.json")
    fun getTopAfter(
            @Path("subreddit") subreddit: String,
            @Query("after") after: String,
            @Query("limit") limit: Int): Single<ListingResponse>

    data class ListingResponse(val data: ListingData)

    data class ListingData(
            val children: List<RedditChildrenResponse>,
            val after: String?,
            val before: String?
    )

    data class RedditChildrenResponse(val data: RedditPost)
}