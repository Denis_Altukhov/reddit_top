package com.altukhov.topreddit.reddit.dagger

import com.altukhov.topreddit.app.dagger.PerActivity
import com.altukhov.topreddit.reddit.RedditActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
@PerActivity
@Subcomponent(modules = [(RedditActivityModule::class)])
interface RedditActivityComponent : AndroidInjector<RedditActivity> {

    @Subcomponent.Builder
    abstract class Builder: AndroidInjector.Builder<RedditActivity>()
}