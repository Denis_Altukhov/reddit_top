package com.altukhov.topreddit.reddit

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import com.altukhov.topreddit.R
import com.altukhov.topreddit.api.NetworkState
import com.altukhov.topreddit.data.RedditPost
import com.altukhov.topreddit.databinding.ActivityRedditBinding
import com.altukhov.topreddit.reddit.ui.PostsAdapter
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_reddit.*
import javax.inject.Inject

class RedditActivity : DaggerAppCompatActivity() {
    companion object {
        val KEY_SUBREDDIT = "subreddit"
    }

    lateinit var binding: ActivityRedditBinding

    @Inject
    lateinit var viewModelFactory: RedditViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reddit)
        binding.viewModel = ViewModelProviders.of(this, viewModelFactory).get(RedditViewModel::class.java)
        initAdapter()
        initSwipeToRefresh()
        initSearch()
        val subreddit = savedInstanceState?.getString(KEY_SUBREDDIT) ?: getString(R.string.default_subreddit)
        binding.viewModel?.showSubreddit(subreddit)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(KEY_SUBREDDIT, binding.viewModel?.currentSubreddit())
    }

    private fun initAdapter() {
        val adapter = PostsAdapter {
            binding.viewModel?.retry()
        }
        list.adapter = adapter
        binding.viewModel?.posts?.observe(this, Observer<PagedList<RedditPost>> {
            adapter.setList(it)
        })
        binding.viewModel?.networkState?.observe(this, Observer {
            adapter.setNetworkState(it)
        })
        binding.viewModel?.postsCount?.observe(this, Observer {
            if (it!!)
                Snackbar.make(root, R.string.footer_msg, Snackbar.LENGTH_LONG).show()
        })
    }

    private fun initSwipeToRefresh() {
        binding.viewModel?.refreshState?.observe(this, Observer {
            swipe_refresh.isRefreshing = it == NetworkState.LOADING
        })
        swipe_refresh.setOnRefreshListener {
            binding.viewModel?.refresh()
        }
    }

    private fun initSearch() {
        input.setOnEditorActionListener({ _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                updatedSubredditFromInput()
                true
            } else {
                false
            }
        })
        input.setOnKeyListener({ _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                val systemService = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                systemService.hideSoftInputFromWindow(input.windowToken, 0)
                updatedSubredditFromInput()
                true
            } else {
                false
            }
        })
    }

    private fun updatedSubredditFromInput() {
        input.text.trim().toString().let {
            if (it.isNotEmpty()) {
                if (binding.viewModel?.showSubreddit(it)!!) {
                    list.scrollToPosition(0)
                    (list.adapter as? PostsAdapter)?.setList(null)
                }
            }
        }
    }
}
