package com.altukhov.topreddit.app.dagger

import android.app.Activity
import com.altukhov.topreddit.reddit.RedditActivity
import com.altukhov.topreddit.reddit.dagger.RedditActivityComponent
import dagger.Binds
import dagger.Module
import dagger.android.ActivityKey
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
@Module(subcomponents = [(RedditActivityComponent::class)])
abstract class AppScBuildersModule {

    @Binds
    @IntoMap
    @ActivityKey(RedditActivity::class)
    abstract fun bindRedditActivityInjectorFactory(builder: RedditActivityComponent.Builder): AndroidInjector.Factory<out Activity>
}