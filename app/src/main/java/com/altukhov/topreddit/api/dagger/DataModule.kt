package com.altukhov.topreddit.api.dagger

import com.altukhov.topreddit.BuildConfig
import com.altukhov.topreddit.api.RedditApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
@Module
class DataModule {

    private val BASE_URL = "https://www.reddit.com"

    @Provides
    @Singleton
    fun provideHttpClient() = OkHttpClient()
            .newBuilder()
            .addInterceptor(HttpLoggingInterceptor().apply {
        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    }).build()


    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Provides
    fun provideRedditApi(retrofit: Retrofit) = retrofit.create(RedditApi::class.java)
}