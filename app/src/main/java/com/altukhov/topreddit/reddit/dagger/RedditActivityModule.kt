package com.altukhov.topreddit.reddit.dagger

import com.altukhov.topreddit.app.dagger.PerActivity
import com.altukhov.topreddit.reddit.RedditViewModelFactory
import com.altukhov.topreddit.repository.RedditPostRepository
import dagger.Module
import dagger.Provides

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
@Module
class RedditActivityModule {

    @Provides
    @PerActivity
    fun provideRedditModelView(redditPostRepository: RedditPostRepository) = RedditViewModelFactory(redditPostRepository)
}