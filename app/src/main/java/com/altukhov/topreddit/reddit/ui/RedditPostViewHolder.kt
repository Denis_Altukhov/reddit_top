package com.altukhov.topreddit.reddit.ui

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.net.Uri
import android.support.customtabs.CustomTabsIntent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.altukhov.topreddit.R
import com.altukhov.topreddit.data.RedditPost
import com.android.databinding.library.baseAdapters.BR



/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
class RedditPostViewHolder(val binding: ViewDataBinding)
    : RecyclerView.ViewHolder(binding.root) {

    fun bind(post: RedditPost?) {
        binding.setVariable(BR.post, post)
        binding.executePendingBindings()
        binding.root.setOnClickListener { post?.url?.let { url ->
            val builder = CustomTabsIntent.Builder()
            builder.setToolbarColor(ContextCompat.getColor(binding.root.context, R.color.colorPrimary))
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(binding.root.context, Uri.parse(url))
        } }
    }

    companion object {
        fun create(parent: ViewGroup): RedditPostViewHolder {
            return RedditPostViewHolder(DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context), R.layout.reddit_post_item, parent, false))
        }
    }
}