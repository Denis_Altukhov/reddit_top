package com.altukhov.topreddit.reddit.ui

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.altukhov.topreddit.R
import com.altukhov.topreddit.api.NetworkState
import com.android.databinding.library.baseAdapters.BR

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
class NetworkStateItemViewHolder(val binding: ViewDataBinding,
                                 private val retryCallback: () -> Unit)
    : RecyclerView.ViewHolder(binding.root) {

    fun bindTo(networkState: NetworkState?) {
        binding.setVariable(BR.retryCallback, retryCallback)
        binding.setVariable(BR.data, networkState)
        binding.executePendingBindings()
    }

    companion object {
        fun create(parent: ViewGroup, retryCallback: () -> Unit): NetworkStateItemViewHolder {
            return NetworkStateItemViewHolder(DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context), R.layout.network_state_item, parent, false), retryCallback)
        }
    }
}