package com.altukhov.topreddit.reddit

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.altukhov.topreddit.repository.RedditPostRepository

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
class RedditViewModelFactory(private val repository: RedditPostRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        modelClass.let {
            if (modelClass.isAssignableFrom(RedditViewModel::class.java)) {
                return RedditViewModel(repository) as T
            }
        }
        throw IllegalArgumentException("Unknown viewModel class")
    }
}