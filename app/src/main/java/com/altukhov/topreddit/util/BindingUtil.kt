package com.altukhov.topreddit.util

import android.databinding.BindingAdapter
import android.databinding.BindingConversion
import android.text.format.DateUtils
import android.widget.ImageView
import com.altukhov.topreddit.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

/**
 * Created by dsa on 12/25/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
object BindingUtil {

    @JvmStatic
    @BindingAdapter("android:src")
    fun setImageUrl(view: ImageView, url: String) {
        if (url.startsWith("http"))
            Glide.with(view.context).load(url).apply(RequestOptions().centerCrop().placeholder(R.drawable.ic_insert_photo_black_48dp))
                    .into(view)
        else
            Glide.with(view.context).clear(view)
    }

    @JvmStatic
    @BindingConversion
    fun convertScore(number: Long): String {
        if (number < 1000) return "" + number
        val exp = (Math.log(number.toDouble()) / Math.log(1000.0)).toInt()
        return "${(number / Math.pow(1000.0, exp.toDouble())).format(1)} ${"kMGTPE"[exp - 1]}"
    }

    fun Double.format(digits: Int) = java.lang.String.format("%.${digits}f", this)

    @JvmStatic
    @BindingConversion
    fun convertTime(time: Long) = DateUtils.getRelativeTimeSpanString(time * 1000, System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS).toString()

}