package com.altukhov.topreddit.repository

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.altukhov.topreddit.api.RedditApi
import com.altukhov.topreddit.data.RedditPost
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
class SubRedditDataSourceFactory (
        private val redditApi: RedditApi,
        private val subredditName: String,
        private val totalItemCount: Int,
        private val compositeDisposable: CompositeDisposable) : DataSource.Factory<String, RedditPost> {
    val sourceLiveData = MutableLiveData<PageKeyedSubredditDataSource>()
    override fun create(): DataSource<String, RedditPost> {
        val source = PageKeyedSubredditDataSource(redditApi, subredditName, totalItemCount, compositeDisposable)
        sourceLiveData.postValue(source)
        return source
    }
}