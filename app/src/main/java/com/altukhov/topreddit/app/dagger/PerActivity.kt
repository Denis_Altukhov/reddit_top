package com.altukhov.topreddit.app.dagger

import javax.inject.Scope

/**
 * Created by dsa on 12/25/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
@Scope
annotation class PerActivity