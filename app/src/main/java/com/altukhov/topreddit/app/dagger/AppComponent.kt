package com.altukhov.topreddit.app.dagger

import com.altukhov.topreddit.api.dagger.DataModule
import com.altukhov.topreddit.app.App
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by dsa on 12/24/17.
 * Author:       Denis Altukhov
 * Email:        dsa@anadeainc.com
 * Company:      Anadea Inc.
 */
@Singleton
@Component(modules = [(AppScBuildersModule::class), (AppModule::class), (DataModule::class), (AndroidSupportInjectionModule::class)])
interface AppComponent {

    fun injectApp(app: App)

}